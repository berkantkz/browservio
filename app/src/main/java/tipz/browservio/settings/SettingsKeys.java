package tipz.browservio.settings;

public class SettingsKeys {
    /* browservio_saver */
    public static final String browservio_saver = "browservio.cfg";
    public static final String centerActionBar = "centerActionBar";
    public static final String defaultHomePage = "defaultHomePage";
    public static final String defaultHomePageId = "defaultHomePageId";
    public static final String defaultSearch = "defaultSearch";
    public static final String defaultSearchId = "defaultSearchId";
    public static final String defaultSuggestions = "defaultSuggestions";
    public static final String defaultSuggestionsId = "defaultSuggestionsId";
    public static final String enableAdBlock = "enableAdBlock";
    public static final String isJavaScriptEnabled = "isJavaScriptEnabled";
    public static final String sendDNT = "sendDNT";
    public static final String showFavicon = "showFavicon";
    public static final String themeId = "themeId";
    public static final String updateTesting = "updateTesting";
}